package com.tutorial.introwebflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntroWebfluxApplication {
    public static void main(String[] args) {
            SpringApplication.run(IntroWebfluxApplication.class, args);
    }
}
