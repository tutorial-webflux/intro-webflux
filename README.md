# Intro WebFlux

## About
Sample project of Spring WebFlux with annotation-based programming model.
- GET All Employees (/employees)
- GET Employee by Id (/employees/{id})
- POST Update Employee (/employees)

## Source
https://www.baeldung.com/spring-webflux